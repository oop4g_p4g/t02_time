////////////////////////////////////////////////////////////////////////
// OOP Tutorial 4: Object Creation using a Time class (version 0: to be modified)
////////////////////////////////////////////////////////////////////////

//--------include libraries
#include <ctime>	
#include <iostream>	
#include <sstream>	
#include <string>	
#include <iomanip>	
using namespace std;

//--------Time class
class Time {
public:
	Time();			//default constructor
	Time(const Time&);	//copy constructor
	~Time();			//destructor
	int getHours() const;	//return a data member value, hrs_
	int getMinutes() const;	//return a data member value, mins_
	int getSeconds() const;	//return a data member value, secs_
	void setTime(int, int, int);	//set the time to 3 given values
	void readInTime();	//input time from user
	void displayTime() const;	//display in 00:00:00 digital clock format
private:
	int hrs_, mins_, secs_;
	long long toSeconds() const;	//return the time in seconds
};

//public functions_______________________________________________

Time::Time() 
: hrs_(0), mins_(0), secs_(0)
{
//	cout << "\n   Calling Time() default constructor!"; 
}
//using memberwise initialisation
Time::Time(const Time& t) 
: hrs_(t.hrs_), mins_(t.mins_), secs_(t.secs_)
{
//	cout << "\n   Calling Time(const Time& t) copy constructor!";
}
Time::~Time() {
//	cout << "\n   Calling ~Time() destructor!";
}
int Time::getHours() const { 	//return a data member value, hrs_
	return hrs_;
}
int Time::getMinutes() const {	//return a data member value, mins_
	return mins_;
}
int Time::getSeconds() const {	//return a data member value, secs_
	return secs_;
}
void Time::setTime(int h, int m, int s) {	//set the time
	hrs_ = h;
	mins_ = m;
	secs_ = s;
}
void Time::readInTime() {  	//read in time form user
	cout << "Enter the hours: ";
	cin >> hrs_;
	cout << "Enter the minutes: ";
	cin >> mins_;
	cout << "Enter the seconds: ";
	cin >> secs_;
}
void Time::displayTime() const {	 //display time (00:00:00)
	const char prev(cout.fill ('0')); 	//save previous fill and set it to new value
	cout << setw(2) << hrs_ << ":"
	     << setw(2) << mins_ << ":"
	     << setw(2) << secs_;
	cout.fill(prev); 	//reset previous fill
}

//private support functions_______________________________________________
long long Time::toSeconds() const {	 //return time in seconds
	return (hrs_ * 3600) + (mins_ * 60) + secs_;
}

//--------end of Time class


//---------------------------------------------------------------------------
int main()
{
	cout << "\n\n__ Time t1\n";
	Time t1;
	t1.displayTime();

	cout << "\n\n__ cout << t1.getHours()\n";
	cout << t1.getHours();

	cout << "\n\n__ t1.displayTime()\n";
	t1.displayTime();

	cout << "\n\n__ t1.setTime(1,2,3)\n";
	t1.setTime(1,2,3);
	t1.displayTime();

	cout << "\n\n__ Time t2(t1)\n";
	Time t2(t1);
	t2.displayTime();

	cout << "\n\n__ t1.readInTime()\n";
	t1.readInTime();
	t1.displayTime();


	cout << endl << endl;
	system("pause");
	return 0;
}
